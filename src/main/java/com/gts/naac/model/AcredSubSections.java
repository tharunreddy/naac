package com.gts.naac.model;

import lombok.*;
import java.math.BigDecimal;
import java.util.Date;


@Data
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AcredSubSections {

    private Long subSectionId;

    /*Modify below field based on the relationship*/
    private AcredSections acredSections;

    private String subSectionNumber;

    private String subSectionName;

    private Integer subSectionSortOrder;

    private String description;

    /*Modify the below field based on the relationship*/
    private BigDecimal catdetId;

    private Boolean isActive;

    private Boolean isVerificationRequired;

    private String reason;

    private Date createdDt;

    private Long createdUser;

    private Date updatedDt;

    private Long updatedUser;
}
