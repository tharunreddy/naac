package com.gts.naac.entities;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@NamedQuery(name="AcredSections.findAll", query="SELECT a FROM AcredSections a")
@Table(name="t_acred_sections")
public class AcredSections implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "pk_acred_section_id")
    private Long sectionId;

    @Column(name = "fk_acred_group_id")
    private Long groupID;

    @Column(name = "acred_section_no")
    private String sectionNumber;

    @Column(name = "acred_section_name")
    private String sectionName;

    @Column(name = "section_sortorder")
    private Integer sectionSortOrder;

    @Column(name = "section_description")
    private String description;

    /*Modify the below field based on the relationship*/
    @JoinColumn(name = "fk_sectiontype_catdet_id")
    private BigDecimal catdetId;

    @Column(name="is_active")
    private Boolean isActive;

    @Column(name="is_verificationrequired")
    private Boolean isVerificationRequired;

    @Column(name = "reason")
    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="created_dt")
    private Date createdDt;

    @Column(name="created_user")
    private Long createdUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="updated_dt")
    private Date updatedDt;

    @Column(name="updated_user")
    private Long updatedUser;
}
