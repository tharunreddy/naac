package com.gts.naac.model;

import lombok.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AcredPointers implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long pointerId;

    /*Modify below field based on the relationship*/
    private Long groupId;

    /*Modify below field based on the relationship*/
    private Long sectionId;

    /*Modify below field based on the relationship*/
    private Long subSectionId;

    private String pointerNumber;

    private String pointerName;

    private Integer pointerSortOrder;

    private String description;

    private Boolean isMandatory;

    private Boolean isActive;

    private Boolean isVerificationRequired;

    private BigDecimal weightage;

    private String reason;

    private Date createdDt;

    private Long createdUser;

    private Date updatedDt;

    private Long updatedUser;

}
