package com.gts.naac.entities;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@NamedQuery(name="AcredGroups.findAll", query="SELECT a FROM AcredGroups a")
@Table(name="t_acred_groups")
public class AcredGroups implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="pk_acred_group_id")
    private Long groupID;

    /*Modify below column based on the relationship*/
    @JoinColumn(name = "fk_acred_template_id")
    private AcredTemplates acredTemplates;

    @Column(name = "acred_group_no")
    private String groupNumber;

    @Column(name = "acred_group_name")
    private String groupName;

    @Column(name = "acred_group_code")
    private String groupCode;

    @Column(name = "group_sortorder")
    private Integer groupSortOrder;

    @Column(name = "group_description")
    private String groupDescription;

    @Column(name="is_active")
    private Boolean isActive;

    @Column(name="is_verificationrequired")
    private Boolean isVerificationRequired;

    @Column(name = "reason")
    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="created_dt")
    private Date createdDt;

    @Column(name="created_user")
    private Long createdUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="updated_dt")
    private Date updatedDt;

    @Column(name="updated_user")
    private Long updatedUser;

}
