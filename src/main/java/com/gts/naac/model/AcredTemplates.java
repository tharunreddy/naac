package com.gts.naac.model;

import lombok.*;
import java.util.Date;

@Data
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AcredTemplates {

    private static final long serialVersionUID = 1L;

    private Long templateId;

    /*Modify below column after confirming the relationship*/
    private String catdetId;

    private String templateName;

    private String templateCode;

    private String templateDesciption;

    private Boolean isActive;

    private String reason;

    private Date createdDt;

    private Long createdUser;

    private Date updatedDt;

    private Long updatedUser;

}