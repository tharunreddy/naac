package com.gts.naac.model;


import lombok.*;
import java.util.Date;

@Data
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AcredPointerFields {

    private Long pointerFieldId;

    /*Modify below column after confirming the relationship*/
    private Long pointerId;

    private String pointerFieldNumber;


    private String description;

    /*Modify below column after confirming the relationship*/
    private Long catdetId;

    private String fieldOptions;

    private String fieldLimitations;

    private String fieldHelpText;

    private boolean isMandatory;

    private String acceptFileType;

    private String sampleFilePathToDownload;

    private boolean isActive;

    private String reason;

    private Date createdDt;

    private Long createdUser;

    private Date updatedDt;

    private Long updatedUser;
}
