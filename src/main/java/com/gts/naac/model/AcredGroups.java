package com.gts.naac.model;

import lombok.*;
import java.util.Date;

@Data
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AcredGroups {

    private Long groupID;

    /*Modify below column after confirming the relationship*/
    private AcredTemplates acredTemplates;

    private String groupNumber;

    private String groupName;

    private String groupCode;

    private Integer groupSortOrder;

    private String groupDescription;

    private Boolean isActive;

    private Boolean isVerificationRequired;

    private String reason;

    private Date createdDt;

    private Long createdUser;

    private Date updatedDt;

    private Long updatedUser;

}
