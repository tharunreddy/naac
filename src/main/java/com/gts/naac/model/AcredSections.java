package com.gts.naac.model;

import lombok.*;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AcredSections {

    private Long sectionId;

    private Long groupID;

    private String sectionNumber;

    private String sectionName;

    private Integer sectionSortOrder;

    private String description;

    /*Modify the below field based on the relationship*/
    private BigDecimal catdetId;

    private Boolean isActive;

    private Boolean isVerificationRequired;

    private String reason;

    private Date createdDt;

    private Long createdUser;

    private Date updatedDt;

    private Long updatedUser;
}
