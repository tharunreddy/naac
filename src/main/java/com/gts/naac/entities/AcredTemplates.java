package com.gts.naac.entities;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@NamedQuery(name="AcredTemplates.findAll", query="SELECT a FROM AcredTemplates a")
@Table(name="t_acred_templates")
public class AcredTemplates implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="pk_acred_template_id")
    private Long templateId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="fk_org_id")
    private Organization organization;

    /*Modify below column after confirming the relationship*/
    @JoinColumn(name="fk_acred_type_catdet_id")
    private String catdetId;

    @Column(name = "template_name")
    private String templateName;

    @Column(name = "template_code")
    private String templateCode;

    @Column(name = "template_description")
    private String templateDesciption;

    @Column(name="is_active")
    private Boolean isActive;

    @Column(name = "reason")
    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="created_dt")
    private Date createdDt;

    @Column(name="created_user")
    private Long createdUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="updated_dt")
    private Date updatedDt;

    @Column(name="updated_user")
    private Long updatedUser;

}