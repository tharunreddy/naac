package com.gts.naac.entities;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@NamedQuery(name="AcredPointers.findAll", query="SELECT a FROM AcredPointers a")
@Table(name="t_acred_pointers")
public class AcredPointers implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "pk_acred_pointer_id")
    private Long pointerId;

    /*Modify below field based on the relationship*/
    @JoinColumn(name = "fk_acred_group_id")
    private Long groupId;

    /*Modify below field based on the relationship*/
    @JoinColumn(name = "fk_acred_section_id")
    private Long sectionId;

    /*Modify below field based on the relationship*/
    @JoinColumn(name = "fk_acred_subsection_id")
    private Long subSectionId;

    @Column(name = "acred_pointer_no")
    private String pointerNumber;

    @Column(name = "acred_pointer_name")
    private String pointerName;

    @Column(name = "pointer_sortorder")
    private Integer pointerSortOrder;

    @Column(name = "pointer_description")
    private String description;

    @Column(name="is_mandatory")
    private Boolean isMandatory;

    @Column(name="is_active")
    private Boolean isActive;

    @Column(name="is_verificationrequired")
    private Boolean isVerificationRequired;

    @Column(name = "weightage")
    private BigDecimal weightage;

    @Column(name = "reason")
    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="created_dt")
    private Date createdDt;

    @Column(name="created_user")
    private Long createdUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="updated_dt")
    private Date updatedDt;

    @Column(name="updated_user")
    private Long updatedUser;

}
