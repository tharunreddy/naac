package com.gts.naac.entities;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


@Entity
@NoArgsConstructor
@AllArgsConstructor
@NamedQuery(name="AcredSubSections.findAll", query="SELECT a FROM AcredSubSections a")
@Table(name="t_acred_subsections")
public class AcredSubSections implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "pk_acred_subsection_id")
    private Long subSectionId;

    /*Modify below field based on the relationship*/
    @JoinColumn(name = "fk_acred_section_id")
    private AcredSections acredSections;

    @Column(name = "acred_subsection_no")
    private String subSectionNumber;

    @Column(name = "acred_subsection_name")
    private String subSectionName;

    @Column(name = "subsection_sortorder")
    private Integer subSectionSortOrder;

    @Column(name = "subsection_description")
    private String description;

    /*Modify the below field based on the relationship*/
    @JoinColumn(name = "fk_subsectiontype_catdet_id")
    private BigDecimal catdetId;

    @Column(name="is_active")
    private Boolean isActive;

    @Column(name="is_verificationrequired")
    private Boolean isVerificationRequired;

    @Column(name = "reason")
    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="created_dt")
    private Date createdDt;

    @Column(name="created_user")
    private Long createdUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="updated_dt")
    private Date updatedDt;

    @Column(name="updated_user")
    private Long updatedUser;
}
