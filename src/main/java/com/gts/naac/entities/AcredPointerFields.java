package com.gts.naac.entities;


import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@NamedQuery(name="AcredPointerFields.findAll", query="SELECT a FROM AcredPointerFields a")
@Table(name="t_acred_pointerfields")
public class AcredPointerFields implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "pk_acred_pointerfield_id")
    private Long pointerFieldId;

    /*Modify below column after confirming the relationship*/
    @JoinColumn(name = "fk_acred_pointer_id")
    private Long pointerId;

    @Column(name = "acred_pointerfield_no")
    private String pointerFieldNumber;

    @Column(name = "acred_pointerfield_description")
    private String description;

    /*Modify below column after confirming the relationship*/
    @JoinColumn(name = "fk_acred_fieldtype_catdet_id")
    private Long catdetId;

    @Column(name = "fieldoptions")
    private String fieldOptions;

    @Column(name = "fieldlimitations")
    private String fieldLimitations;

    @Column(name = "fieldhelptext")
    private String fieldHelpText;

    @Column(name = "is_mandatory")
    private boolean isMandatory;

    @Column(name = "acceptfiletype")
    private String acceptFileType;

    @Column(name = "samplefilepathtodownload")
    private String sampleFilePathToDownload;

    @Column(name="is_active")
    private boolean isActive;

    @Column(name = "reason")
    private String reason;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="created_dt")
    private Date createdDt;

    @Column(name="created_user")
    private Long createdUser;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name="updated_dt")
    private Date updatedDt;

    @Column(name="updated_user")
    private Long updatedUser;
}
